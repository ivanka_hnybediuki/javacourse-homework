package homeworks.homework_1;

import java.util.Formatter;

import static utils.Print.print;

public class RandomNumberGame {
    public static String name = "";
    public static boolean isTheCorrectNumber(int playerNumber, int secretNumber) {
        if (playerNumber < secretNumber) {
            printToSmallMessage();
            return false;
        } else if (playerNumber > secretNumber) {
            printToBigMessage();
            return false;
        } else {
            printCorrectNumberMessage();
            return true;
        }
    }

    public static void printToSmallMessage() {
        String TOO_SMALL_NUMBER_MESSAGE = "Your number is too small. Please, try again.";
        print.println(TOO_SMALL_NUMBER_MESSAGE);
    }

    public static void printToBigMessage() {
        String TOO_BIG_NUMBER_MESSAGE = "Your number is too big. Please, try again.";
        print.println(TOO_BIG_NUMBER_MESSAGE);
    }
    public static void printCorrectNumberMessage() {
        Formatter formatter = new Formatter();
        Formatter VICTORY_MESSAGE = formatter.format("Congratulations, %s!", name);
        print.println(VICTORY_MESSAGE);
    }
}
