package homeworks.homework_1;

import java.util.Random;

import static utils.CustomScanner.scanner;
import static utils.Print.print;

public class HIstoricEvents {
    //The list was genereted by ChatGPT, I didn`t check those events)
    private static String[][] historivEvents= {
            {"When did Mount St. Helens erupt?", "When did John Lennon die?"},
            {"When did the Iran-Iraq War start?", "When did the first space shuttle launch?"},
            {"When did the Solidarity movement start in Poland?", "When did the AIDS virus identified?"},
            {"When did the Falklands War start?", "When did the compact disc introduced?"},
            {"When did the first mobile phone introduced?", "When did the Ethiopian famine occur?"},
            {"When did the Chernobyl disaster occur?", "When did the Berlin Wall fall?"},
            {"When did Nelson Mandela released from prison?", "When did the Gulf War start?"},
            {"When did the Maastricht Treaty signed?", "When did the first World Wide Web software released?"},
            {"When did the Oslo Accords signed?", "When did the Rwandan genocide occur?"},
            {"When did the Dayton Agreement signed?", "When did the Dolly the sheep cloned?"},
            {"When did Hong Kong handed over to China?", "When did the Kyoto Protocol signed?"},
            {"When did the first Harry Potter book published?", "When did the 9/11 terrorist attacks occur?"},
            {"When did the Euro currency introduced?", "When did the invasion of Iraq occur?"},
            {"When did the Indian Ocean earthquake and tsunami occur?", "When did the iPhone released?"},
            {"When did the global financial crisis start?", "When did the Arab Spring begin?"},
            {"When did the Costa Concordia disaster occur?", "When did the Boston Marathon bombings occur?"},
            {"When did the Edward Snowden leak occur?", "When did the MH17 plane crash occur?"},
            {"When did the Charlie Hebdo shooting occur?", "When did the Iran nuclear deal signed?"},
            {"When did the Paris attacks occur?", "When did the Brexit referendum occur?"},
            {"When did the Zika virus outbreak occur?", "When did Donald Trump elected US president?"},
    };

    private static String name = "";


    public static void main(String[] args) {
        String GREETING_MESSAGE = "Let the game begin! ";

        Random random = new Random();
        int secretNumber = random.nextInt(0, 20);
        int correctAnswer = getYearFromIndex(secretNumber);
        int questionNumber = random.nextInt(0, 1);

        String question = historivEvents[secretNumber][questionNumber];

        print.printf("%s\n", GREETING_MESSAGE);

        print.println("Please enter your name bellow");

        RandomNumberGame.name = scanner.nextLine();

        print.println(question);

        while (true) {
            try{
                int playerNumber = scanner.nextInt();
                if (RandomNumberGame.isTheCorrectNumber(playerNumber, correctAnswer)) {
                    break;
                }
            } catch (Exception exp){
                scanner.nextLine();
                print.println("Please enter your number ...");
            }
        }

    }

    public static int getYearFromIndex(int secretNumber){
        int startYear = 1980;
        return startYear + secretNumber;
    }
}
