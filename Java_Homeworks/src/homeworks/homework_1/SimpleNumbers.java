package homeworks.homework_1;

import static utils.CustomScanner.scanner;
import static utils.Print.print;

import java.util.Arrays;
import java.util.Formatter;
import java.util.Random;

public class SimpleNumbers {

    private static int [] storedNumbers = new int[100];
    public static void main(String[] args) {
        String GREETING_MESSAGE = "Let the game begin!";

        Random random = new Random();

        print.printf("%s\n", GREETING_MESSAGE);

        print.println("Please enter your name bellow");

        RandomNumberGame.name = scanner.nextLine();

        int secretNumber = random.nextInt(0, 100);

        int index = 0;

        print.println("Please enter your number ...");
        while (true) {
            try{
                int playerNumber = scanner.nextInt();
                storedNumbers[index] = playerNumber;
                index++;
                if (RandomNumberGame.isTheCorrectNumber(playerNumber, secretNumber)) {
                    printStoredNumbers();
                    break;
                }
            } catch (Exception exp){
                scanner.nextLine();
                print.println("Please enter your number ...");
            }
        }

    }


    private static void printStoredNumbers() {
        Arrays.sort(storedNumbers);

        print.print("Your numbers: ");
        for (int num : storedNumbers) {
            if(num != 0) print.printf("%d ", num);
        }
    }
}
